﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButtonHandler : AbstractBasicButtonHandler
{
    [SerializeField] private GameObject _mainMenu = null; // ссылка на главное меню
    [SerializeField] private GameObject _settingsMenu = null; // ссылка на меню настроек
    [SerializeField] private GameObject _titlesMenu = null;   // ссылка на меню титры


    private void Awake()
    {
        OpenMenu(_mainMenu);   // при запуске сцены, активируем главное меню
    }



    public override void ClickOkButton()
    {
        Debug.Log("Click Ok");

        // надо будет прописать отдельный if для меню настроек

        if (_currentActiveMenu != _mainMenu)   // если не находимся в главном меню, выходим в него
        {
            SelectMenu(_mainMenu);
        }

    }

    public override void ClickCancelButton()
    {
        Debug.Log("Click Cancel");

        if (_currentActiveMenu != _mainMenu)  // если не находимся в главном меню, выходим в него
        {
            SelectMenu(_mainMenu);
        }

    }

    public void ClickPlayButton()
    {
        Debug.Log("Click Play");

        SceneManager.LoadScene("TestSoldering");
        // загрузка игровой сцены


    }

    public void ClickSettingsButton()
    {
        Debug.Log("Click Settings");

        SelectMenu(_settingsMenu);     
    }

    public void ClickTitlesButton()
    {
        Debug.Log("Click Titles");

        SelectMenu(_titlesMenu);
    }

    public void ClickExitButton()
    {
        Debug.Log("Click Exit");

        Application.Quit();
    }


    
}
