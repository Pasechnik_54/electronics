﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class AbstractBasicButtonHandler : MonoBehaviour
{
    protected GameObject _currentActiveMenu = null;  
    

    public virtual void ClickOkButton()    // реакция на кнопку OK, может переопределяться в наследниках
    {
        Debug.Log("Click OK");
    }

    public virtual void ClickCancelButton() // реакция на кнопку Cancel, может переопределяться в наследниках
    {
        Debug.Log("Click Cancel");
    }



    protected void CloseMenu( GameObject menu)  // метод закрывает указанное меню
    {
        menu.SetActive(false);
        _currentActiveMenu = null;

        Debug.Log("CloseMenu()");
    }

    protected void OpenMenu(GameObject menu)    // метод открывает указанное меню
    {
        menu.SetActive(true);
        _currentActiveMenu = menu;

        Debug.Log("OpenMenu()");
    }

    protected void SelectMenu(GameObject menu)
    {
        CloseMenu(_currentActiveMenu);
        OpenMenu(menu);
    }
}
