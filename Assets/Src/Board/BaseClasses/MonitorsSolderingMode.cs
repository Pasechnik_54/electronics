﻿using System;

using UnityEngine;

using ObjectNet = System.Object;

namespace Soldering
{
    public abstract class MonitorsSolderingMode : AbstractComponentForAddingInGameObject, IMonitorsSolderingMode
    {
        [SerializeField] protected SolderSwitch _solderSwitch = null;
        public SolderSwitch SolderSwitch 
        { set { if (value is ISolderSwitch) _solderSwitch = value;} }

        protected bool _solderingModeActive = false;


        protected virtual void SubscribeForEventsSwitchingSolderMode()
        {
            _solderSwitch.OnSolderingBoardMode += new EventHandler(OnSolderingMode);
            _solderSwitch.OffSolderingBoardMode += new EventHandler(OffSolderingMode);
        }

        public virtual void UnsubscribeForEventsSwitchingSolderMode()
        {
            _solderSwitch.OnSolderingBoardMode -= OnSolderingMode;
            _solderSwitch.OffSolderingBoardMode -= OffSolderingMode;
        }

        protected virtual void OnSolderingMode(ObjectNet sender, EventArgs args)
        {            
            _solderingModeActive = true;
        }

        protected virtual void OffSolderingMode(ObjectNet sender, EventArgs args)
        {
            _solderingModeActive = false;
        }

    }
}
