﻿
using UnityEngine;

namespace Soldering
{
    public abstract class AbstractComponentForAddingInGameObject : MonoBehaviour
    {
        protected abstract void GetRequiredComponents();
    }
}
