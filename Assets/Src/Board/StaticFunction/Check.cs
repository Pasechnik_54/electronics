﻿
using System;

using UnityEngine;

using ObjectNet = System.Object;
using ObjectUnity = UnityEngine.Object;

namespace Checking
{  
    // класс для повторяющихся методов проверки объектов, связанных с пайкой
    public  static class Check
    {
        // проверка ссылки на null
        public static bool CheckingToNullOk(ObjectNet checkingObj, MonoBehaviour sender) 
        {
            if (checkingObj == null)
            {
                Debug.Log(sender.ToString() + " !!_reference_null");
                return false;
            }
            else
            {
                return true;
            }
        }

        
        // проверка ссылки на null c выводом заданного сообщения
        public static bool CheckingToNullOk(ObjectNet checkingObj, MonoBehaviour sender, string message) 
        {
            if (checkingObj == null)
            {
                Debug.Log(sender.ToString() + " " + message);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
