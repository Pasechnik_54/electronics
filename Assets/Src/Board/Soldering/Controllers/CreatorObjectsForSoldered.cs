﻿

using UnityEngine;

namespace Soldering
{
    public class CreatorObjectsForSoldered : MonoBehaviour
    {
        [SerializeField] private GameObject _inputPrefab = null;        

        [SerializeField] private GameObject _temperatureScale = null;

        [SerializeField] private GameObject _messageManager = null;

        [SerializeField] private GameObject _switchSolderMode = null;


        private Vector3 _position_1 = new Vector3(4, 0, -1);
        private Vector3 _position_2 = new Vector3(-4, 0, -1);
        private Vector3 _position_3 = new Vector3(0, 1, -1);

        private Vector3[] _startPosition = new Vector3[3];


        private void Awake()
        {
            _startPosition[0] = _position_1;
            _startPosition[1] = _position_2;
            _startPosition[2] = _position_3;

            CreateObjects();
        }
                
        
        
        private void CreateObjects()
        {
            for(int i = 0; i < _startPosition.Length; i++ )
            {
                GameObject createdObject = Instantiate(_inputPrefab, _startPosition[i],
                                                   this.transform.rotation, this.transform);

                // передаем ссылку на переключатель и на менеджер сообщений в компонент состояния пайки
                SolderState currentSolderState = createdObject.GetComponent<SolderState>();

                currentSolderState.SolderSwitch = _switchSolderMode.GetComponent<SolderSwitch>();
                currentSolderState.Messanger = _messageManager.GetComponent<SolderMessageManager>();


                // передаем ссылку на шкалу температуры и переключатель в контроллер пайки 
                SolderController solderingController = createdObject.GetComponent<SolderController>();

                TemperatureScale indicatorTemperature = _temperatureScale.GetComponent<TemperatureScale>();
                solderingController.IndicatorTemperature = indicatorTemperature;
                

                solderingController.SolderSwitch = _switchSolderMode.GetComponent<SolderSwitch>();

                


                // передаем ссылку на  переключатель в компонент нагрева
                Heating currentHeating = createdObject.GetComponent<Heating>();

                currentHeating.SolderSwitch = _switchSolderMode.GetComponent<SolderSwitch>();


                // передаем ссылку на созданный компонент в индикатор температуры
                SolderProcess currentSolderProcess = createdObject.GetComponent<SolderProcess>();
                
                ((ITemperatureIndicator)indicatorTemperature).TransmitCurrentParametersForIndication(currentHeating, currentSolderProcess);
            }
        }
    }
}
