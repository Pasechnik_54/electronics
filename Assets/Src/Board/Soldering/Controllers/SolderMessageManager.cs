﻿
using System;

using UnityEngine;


namespace Soldering
{
    // компонент, отвечающий за показ сообщения о результатах пайки
    public class SolderMessageManager : MonoBehaviour, IMessanger
    {
        
        [SerializeField] private GameObject _prefabMessageOkSoldering = null;   // задать в Inspector
        [SerializeField] private GameObject _prefabMessageElemenAlreadySoldered = null;
        


        public void ShowMessageOkSoldering()
        {
            ShowSelectedMessage(_prefabMessageOkSoldering);
        }

        public void ShowMessageElemenAlreadySoldered()
        {
            ShowSelectedMessage(_prefabMessageElemenAlreadySoldered);
        }


        [SerializeField] private float _delayMessageDelete_s;    // задать в Inspector

        private void ShowSelectedMessage(GameObject selectedMessage)
        {
            GameObject message = Instantiate(selectedMessage, this.transform);

            Destroy(message, _delayMessageDelete_s);
        }

    }
}
