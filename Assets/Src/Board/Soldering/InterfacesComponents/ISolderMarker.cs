﻿
namespace Soldering
{
    public interface ISolderMarker
    {
        void MarkedOn();
        void MarkedOff();
    }
}
