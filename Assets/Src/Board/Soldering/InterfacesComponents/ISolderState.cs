﻿

namespace Soldering
{
    public interface ISolderState 
    {
        SolderState.State CurrentState { get; set; }
    }
}
