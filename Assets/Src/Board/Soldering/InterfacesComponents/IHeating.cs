﻿
namespace Soldering
{
    public interface IHeating
    {
        bool _HeaterOn { get; }
        float CurrentTemperature { get; }
        float MinTemperature { get; }
        float MaxTemperature { get; }

        void CalculationTemperature();       
    }
}
