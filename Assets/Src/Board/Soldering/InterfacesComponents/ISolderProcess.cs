﻿

namespace Soldering
{
    public interface ISolderProcess
    {
        float MinSolderingOkTemperature { get; }
        float MaxSolderingOkTemperature { get; }

        void CalculationSoldering();
    }
}
