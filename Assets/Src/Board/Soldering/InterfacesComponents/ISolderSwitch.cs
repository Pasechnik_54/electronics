﻿
namespace Soldering
{
    public interface ISolderSwitch
    {
        bool SolderingModeInBoardOn { get; }
    }
}
