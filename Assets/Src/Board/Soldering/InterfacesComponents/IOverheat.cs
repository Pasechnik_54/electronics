﻿using UnityEngine;

namespace Soldering
{
    public interface IOverheat
    {
        void ReactionOfOverheating();
    }
}
