﻿


namespace Soldering
{
    public interface ITemperatureIndicator
    {
        void TransmitCurrentParametersForIndication(IHeating heatingComponent, 
                                                ISolderProcess solderProcessComponent);
        void IndicateTemperature();
    }
}
