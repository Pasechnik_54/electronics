﻿

namespace Soldering
{
    public interface IMarker    // интерфейс для выделения объектов (подсветка)
    {
        void MarkerOn();
        void MarkerOff();
    }
}
