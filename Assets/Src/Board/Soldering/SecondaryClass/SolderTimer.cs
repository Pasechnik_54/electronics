﻿
using UnityEngine;

namespace Soldering
{   
    // класс для расчета времени пайки
    public class SolderTimer 
    {
        private bool _timerCountRun = false;
        private float _timeStartCount = 0;

        public void CountTime(ref float countedTime)
        {
            if (_timerCountRun == false)  // если не начали отсчет пайки, запускаем его
                StartTimerCounter();
            else if (_timerCountRun == true)
                countedTime = CalculatedTimeSoldering();
        }

        private void StartTimerCounter()
        {
            _timerCountRun = true;        // запускаем остчет времени
            _timeStartCount = Time.time;
        }

        private float CalculatedTimeSoldering()
        {
            return Time.time - _timeStartCount;  // возвращаем время пайки
        }

        public void ClearCounterTime(ref float countedTime)
        {
            _timerCountRun = false;
            countedTime = 0;
            _timeStartCount = 0;
        }
    }
}
