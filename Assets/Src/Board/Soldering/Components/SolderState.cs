﻿
using System;

using UnityEngine;

using ObjectNet = System.Object;

namespace Soldering
{    
    // компонент отвечает за состояния пайки объекта и связанное с этим поведение
    [AddComponentMenu("User_Soldering/StateSolder")]
    public class SolderState : MonitorsSolderingMode, ISolderState, IAllowedDrag
    {

        private IMessanger _messanger = null;        
        public IMessanger Messanger
        { set { _messanger = value; } }

        private ISolderMarker solderMarker = null;
        public ISolderMarker SolderMarker
        { set { solderMarker = value; } }

        [SerializeField] private State _currentState;
        public State CurrentState 
        { 
            get { return _currentState; }

            set
            {
                if (_currentState == State.Ok)   // нельзя изменить свойство, если элемен уже припаян
                    return;
                else if (value == State.Ok)
                {
                    _currentState = value;
                    solderMarker.MarkedOff();
                    _messanger.ShowMessageOkSoldering();
                }
                else
                    _currentState = value;
               
            } 
        }

        public enum State
        {
            Ok,
            NotSoldered,
            Process
        }


        

        private void Start()
        {
            GetRequiredComponents();

            SubscribeForEventsSwitchingSolderMode();
        }

        protected override void GetRequiredComponents()
        {
            solderMarker = GetComponent<ISolderMarker>();
        }



        // Обработка событий переключения режимов пайки 
        protected override void OnSolderingMode(ObjectNet sender, EventArgs args)
        {
            _solderingModeActive = true;

            CurrentState = State.Process;

            if (_currentState != State.Ok)
                solderMarker.MarkedOn();
        }

        protected override void OffSolderingMode(ObjectNet sender, EventArgs args) 
        {
            _solderingModeActive = false;

            if (CurrentState != State.Ok) // если компонент не был припаян, делаем его не припаянным
                CurrentState = State.NotSoldered;

            solderMarker.MarkedOff();
        }




        private void OnMouseDown()
        {
            if (_currentState == State.Ok && _solderingModeActive)
                _messanger.ShowMessageElemenAlreadySoldered();
        }
        


        
        public bool GetAllowedDrag()    // метод, разрешающий перемещение если компонент не припаян (и наоборот)
        {
            if (_currentState == State.NotSoldered)
                return true;            
            else
                return false;
        }
    }

}
