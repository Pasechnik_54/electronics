﻿
using UnityEngine;

namespace Soldering
{
    [AddComponentMenu("User_Soldering/Overheat")]
    public class Overheat : AbstractPartialSolderingProcess, IOverheat
    {
        SolderState _solderState = null;



        private void Start()
        {
            GetRequiredComponents();
        }

        protected override void GetRequiredComponents()
        {
            _solderState = GetComponent<SolderState>();
        }



        public void ReactionOfOverheating()
        {
            _solderState.UnsubscribeForEventsSwitchingSolderMode(); // перед уничтожением объекта, отписываемся от событий

            Destroy(this.gameObject, 0.5f);
            Debug.Log("Element overheated");
        }
    } 
}
