﻿
using UnityEngine;
using UnityEngine.UI;

namespace Soldering
{
    // компонент отвечает за правила индикации температуры
    public class TemperatureScale : AbstractPartialSolderingProcess, ITemperatureIndicator
    {       
        private IHeating _heating = null;
        private ISolderProcess _solderProcess = null;

        [SerializeField] private GameObject _indicatorLine = null;  // назначить в Inspector
        [SerializeField] private GameObject _indicatorMask = null;

        private Image _imageScale = null;
        private RectTransform _maskRectTransform = null;

        [SerializeField] private float _widtchLine;
        
        private float _rangeIndication;
        private const float START_INDICATION_OFFSET = 50;



        private void Start()
        {
            GetRequiredComponents();

            _widtchLine = _maskRectTransform.rect.width;
            _rangeIndication = (_heating.MaxTemperature - _heating.MinTemperature) + START_INDICATION_OFFSET;
        }

        protected override void GetRequiredComponents()
        {
            _imageScale = _indicatorLine.GetComponent<Image>();
            _maskRectTransform = _indicatorMask.GetComponent<RectTransform>();
        }
        


        
        private Vector3 _targetMovingVector;  
        
        // вызывается в Update() !! при необходимости, оптимизировать
        public void IndicateTemperature()   
        {            
            _targetMovingVector.x = CalculatedTargetPositionOfTemperature();

            _indicatorLine.transform.localPosition = _targetMovingVector;  // задаем координаты для текущего кадра

            _imageScale.color = ColorDependToTemp();   // задаем цвет для текущего кадра
        }
        
        private float CalculatedTargetPositionOfTemperature()
        {
            float offsetLine = _heating.CurrentTemperature - _heating.MaxTemperature;
            float scalingForWidth = _widtchLine / _rangeIndication;

            return offsetLine * scalingForWidth;
        }
        


        // изменение цвета шкалы
        Color _coldColor = new Color(0, 0, 1, 1);    // blue
        Color _solderOkColor = new Color(0, 1, 0, 1);  // green
        Color _hotColor = new Color(1, 0, 0, 1);     // red
        
        private Color ColorDependToTemp() 
        {
           if(_heating.CurrentTemperature < _solderProcess.MinSolderingOkTemperature)
                return _coldColor;
           else if(_heating.CurrentTemperature > _solderProcess.MaxSolderingOkTemperature)
                return _hotColor;
           else
                return _solderOkColor;
        }



        // интерфейсная функция для передачи извне ссылок на нужные для индикации параметров компоненты
        public void TransmitCurrentParametersForIndication(IHeating selectedHeating,
                                                        ISolderProcess selectedSolderProcess)
        {
            _heating = selectedHeating;
            _solderProcess = selectedSolderProcess;
        }
    }
}
