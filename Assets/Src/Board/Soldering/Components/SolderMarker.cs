﻿
using UnityEngine;

namespace Soldering
{   // компонент, отвечающий за выделение объекта
    [AddComponentMenu("User_Soldering/SolderMarker")]
    public class SolderMarker : AbstractPartialSolderingProcess, ISolderMarker
    {        
        private Color _markedColor = new Color(0, 1, 1, 0.9F);  // цвет выделения
        private Color _defaultColor = new Color(1, 1, 1, 1);    // цвет по умолчанию

        private SpriteRenderer _currentRenderer = null; 


        private void Start()
        {
            GetRequiredComponents();
        }

        protected override void GetRequiredComponents()
        {
            _currentRenderer = GetComponent<SpriteRenderer>();
        }



        public void MarkedOn()  // включить выделение паечного компонента
        {
            _currentRenderer.color = _markedColor;
        }

        public void MarkedOff() // отключить выделение паечного компонента
        {
            _currentRenderer.color = _defaultColor;
        }
    }
}
