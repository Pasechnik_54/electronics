﻿
using System.Collections;

using UnityEngine;

namespace Soldering
{       
    // компонент, отвечающий за правила подогрева-охлаждения объекта
    [AddComponentMenu("User_Soldering/Heating")]
    public class Heating : MonitorsSolderingMode, IHeating
    {
        private ISolderState _solderState = null;

        private const float MIN_TEMPERATURE = 250;
        public float MinTemperature { get { return MIN_TEMPERATURE; }  }
        
        private const float MAX_TEMPERATURE = 400;
        public float MaxTemperature { get { return MAX_TEMPERATURE; } }

        private float _currentTemperature;
        public float CurrentTemperature
        {
            get { return _currentTemperature;}

            private set
            {
                if (value <= MAX_TEMPERATURE && value >= MIN_TEMPERATURE)
                    _currentTemperature = value;
            }
        }


        private void Start()
        {
            SubscribeForEventsSwitchingSolderMode();
            GetRequiredComponents();

            CurrentTemperature = MIN_TEMPERATURE;
        }

        protected override void GetRequiredComponents()
        {
            _solderState = GetComponent<ISolderState>();
        }



        public bool _HeaterOn {get; private set; } 

        public void OnMouseDrag() 
        {
            // при удерживании вызывается каждый кадр!!
            // при удерживании на коллайдере в процессе пайки, вкл нагрев
            if (_solderState.CurrentState == SolderState.State.Process)
                _HeaterOn = true;
        }

        public void OnMouseUp() 
        {
            _HeaterOn = false;
        }
        



        [SerializeField] private float _speedHeating;            // скорости процессов задать в префабе в Inspector
        [SerializeField] private float _speedCoolingOnSoldering; 
        [SerializeField] private float _speedCoolingOffSoldering;


        public void CalculationTemperature()   // вызывается в FixedUpdate()!! при необходимости оптимизировать
        {
            if (_solderingModeActive == false)
                ProcessCoolingNoSolderingMode();
            else if (_solderingModeActive)
            {
                if (_solderState.CurrentState == SolderState.State.Process)
                {
                    if (_HeaterOn == true)
                        ProcessHeating();
                    else
                        ProcessCoolingInSoldering();
                }
                else
                    ProcessCoolingAfterSoldering();
            }
        }


       private void ProcessHeating()    
        {                        
           if(_currentTemperature <= MAX_TEMPERATURE)
            CurrentTemperature += _speedHeating;
        }


        private void ProcessCoolingInSoldering()    
        {
            if (_currentTemperature >= MIN_TEMPERATURE)
                CurrentTemperature -= _speedCoolingOnSoldering;
        }


        private void ProcessCoolingAfterSoldering()  
        {
            StartCoroutine(CoolingWitchDelay());
        }


        [SerializeField] private float _delayForSolderingComplete;   // задать в Inspector

        private IEnumerator CoolingWitchDelay()  // задержка охлаждения
        {
            yield return new WaitForSeconds(_delayForSolderingComplete); // ждем
                CurrentTemperature -= _speedCoolingOffSoldering;
        }


        private void ProcessCoolingNoSolderingMode() 
        {
            CurrentTemperature -= _speedCoolingOffSoldering;
        }

    }
}
