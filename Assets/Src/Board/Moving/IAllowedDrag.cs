﻿

namespace Soldering
{
    public interface IAllowedDrag   // интерфейс для разрешения на перетаскивание объекта
    {
       bool GetAllowedDrag(); 
    }
}
