﻿
using UnityEngine;

using Checking;

namespace Soldering
{
    // компонент для перетаскивания объекта при касании
    public class DragComponentOnTouch : MonoBehaviour
    {
        private bool _allowedDrag = false; // разрешение на перетаскивание(по умолчанию запрещено)

        IAllowedDrag _allowed;   // ссылка на объект, реализующий разрешение на перетаскивание

        private Vector3 _currentTouchPosition = new Vector3();  // текущая позиция нажатия на экран



        private void Start()
        {
            _allowed = GetComponent<IAllowedDrag>();  
            Check.CheckingToNullOk(_allowed, this);
        }

        public void OnMouseDown()   // при нажатии на GameObject(collider) проверяем разрешение на перетаскивание
        {
            _allowedDrag = _allowed.GetAllowedDrag();
        }

        public void OnMouseDrag()   // перетаскиваем Gameobject, при нажатии на него 
        {
            DragComponentMoveTouch(_allowedDrag);  
        }                                               
        
        
        public void DragComponentMoveTouch(bool access)
        {
            if (access)    // если разрешено, претаскиваем GameObject
            {
                _currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _currentTouchPosition.z = -1;

                transform.localPosition = _currentTouchPosition;
            }
        }

       

    }
}
